﻿using System;

namespace HomeTasks_Lesson8
{
    class MethodForDelegate
    {
        public static void FirrsSecondCounter(bool isFirst)
        {
            if (isFirst)
            {
                Console.WriteLine("First");
            }
            else
            {
                Console.WriteLine("Second");
            }
        }
    }
}
