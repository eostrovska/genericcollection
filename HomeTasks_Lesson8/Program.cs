﻿using HomeTasks_Lesson8;
using System;

namespace DelegateArray
{
    public delegate void FirstSecondCountDelegate(bool isFirst);

    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Create an array of delegates. Non-even methods assocciated with delegates from an array,show to the console message 'FIRST'." +
                              "Even methods assocciated with delegates from the array, show to the console message 'SECOND'.");
            Console.WriteLine(new string('-', 60));

            FirstSecondCountDelegate delegate1 = new FirstSecondCountDelegate(MethodForDelegate.FirrsSecondCounter);
            FirstSecondCountDelegate delegate2 = new FirstSecondCountDelegate(MethodForDelegate.FirrsSecondCounter);
            FirstSecondCountDelegate delegate3 = new FirstSecondCountDelegate(MethodForDelegate.FirrsSecondCounter);
            FirstSecondCountDelegate delegate4 = new FirstSecondCountDelegate(MethodForDelegate.FirrsSecondCounter);
            FirstSecondCountDelegate delegate5 = new FirstSecondCountDelegate(MethodForDelegate.FirrsSecondCounter);
            FirstSecondCountDelegate[] delegateArray = new FirstSecondCountDelegate[5];

            delegateArray[0] = delegate1;
            delegateArray[1] = delegate2;
            delegateArray[2] = delegate3;
            delegateArray[3] = delegate4;
            delegateArray[4] = delegate5;

            for (int i = 0; i < delegateArray.Length; i++)
            {
                delegateArray[i].Invoke(i % 2 == 0);
            }
            Console.ReadKey();
        }
    }
}
