﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            var temp = new CustomDictionary<int, string>();

            Console.WriteLine("Create the Dictionary class <TKey, TValue>. Implement in the simplest approximation the ability to use its instance is similar" +
                              "to an instance of the Dictionary class from the System.Collections.Generic namespace." + 
                              "Minimum required interface interaction with the instance, must include the method of adding pairs of elements," +
                              "indexer to get the value of the element at the specified index and the property, readonly to get the total number of pairs of elements.");
            Console.WriteLine(new string('-',60));

            temp.Add(0, "Korovka");
            temp.Add(1, "Sobachka");
            temp.Add(2, "Belochka");
            temp.Add(3, "Kotik");

            Console.WriteLine("The number of Custom Dictionary elements is {0}", temp.Size);
            Console.WriteLine("The value of Key {0} is {1}", 3, temp.GetElementByKey(3));

            Console.ReadKey();
        }
    }
}
