﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    class CustomDictionary<TKey, TValue> : IEnumerable
    {
        private List<Pair<TKey, TValue>> _list = new List<Pair<TKey, TValue>>();

        public int Size
        {
            get { return _list.Count; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        public void Add(TKey key, TValue value)
        {

            if (_list.Any(x => x.Key.Equals(key)))
            {
                throw new ArgumentException("Key is already being used");
            } else if (key == null)
            {
                throw new ArgumentNullException("Key", "Key cannot be null");
            }
            else
            {
                _list.Add(new Pair<TKey, TValue>(key, value));
            }
        }

        public TValue GetElementByKey(TKey key)
        {
            return _list.Single(x => x.Key.Equals(key)).Value;
        }


    }
}
