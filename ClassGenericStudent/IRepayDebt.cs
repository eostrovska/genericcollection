﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGenericStudent
{
    interface IRepayDebt<out T>
    {
        void RepayDebt();
    }
}
