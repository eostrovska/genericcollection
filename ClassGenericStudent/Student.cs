﻿using System;

namespace ClassGenericStudent
{
    class Human { }
    class User : Human { }
    public class Student<T> : IRepayDebt<T>, IBorrow<T>
    {
        private T argument;

        public Student(T argument)
        {
            this.argument = argument;
        }

        public void RepayDebt()
        {
            Console.WriteLine("Repay Debt.");
        }

        public void Borrow()
        {
            Console.WriteLine("Borrow.");
        }
    }
}
