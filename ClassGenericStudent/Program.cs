﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGenericStudent
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Create a class-generic Student. A Student implements two interfaces. One interface has a method of 'Return the debt' and works on covariance." +
                               "The second interface has the method of 'Borrow' and works on contravariance.");
            Console.WriteLine(new string('-', 60));

            User user = new User();
            IRepayDebt<Human> human = new Student<User>(user);

            Human man = new User();
            IBorrow<User> student = new Student<Human>(man);

            Console.ReadKey();
        }
    }
}
